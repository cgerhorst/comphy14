#include <functional>
#include <tuple>
#include <vector>

#include <Eigen/Dense>

#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>>
rungeKutta(std::function<Eigen::VectorXd(double t, Eigen::VectorXd x)> f,
           Eigen::VectorXd x0, size_t N, double t0, double T,
           Eigen::MatrixXd a, Eigen::VectorXd b, Eigen::VectorXd c);

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
rungeKutta(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
           std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
           Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T,
           Eigen::MatrixXd a, Eigen::VectorXd b, Eigen::VectorXd c);

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
forwardEuler(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
             std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
             Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T);

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
rungeKutta4(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
            std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
            Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T);

#endif
