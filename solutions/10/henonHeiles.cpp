#include <cmath>
#include <cstdio>
#include <iostream>

#include "rungeKutta.h"
#include "symplectic.h"

typedef std::function<std::tuple<std::vector<double>,
                                 std::vector<Eigen::VectorXd>,
                                 std::vector<Eigen::VectorXd>>(
    std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
    std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr, const Eigen::VectorXd& r0,
    Eigen::VectorXd v0, size_t N, double t0, double T)> Integrator;

void henonHeiles(Integrator integrator, double pE)
{
	const size_t blocks = 10;
	const size_t N = 1000000;
	const double T_block = 100000;
	const size_t trajectory_T = 100;
	const size_t trajectory_N = N / T_block * trajectory_T;

	const double E = 1. / pE;
	const double y = -0.2;
	const double vy = 0;

	Eigen::VectorXd x0 = Eigen::Vector2d(0, y);
	Eigen::VectorXd v0 = Eigen::Vector2d(sqrt(2 * E - vy * vy - y * y + 2. / 3. * y * y * y), vy);

	std::vector<double> t;
	std::vector<Eigen::VectorXd> x;
	std::vector<Eigen::VectorXd> v;

	char name[256];
	sprintf(name, "poincare-%.0f.dat", pE);
	auto file = std::fopen(name, "w");

	for (int block = 0; block < blocks; block++)
	{
		double t0 = block * T_block;
		double T = t0 + T_block;
		std::tie(t, x, v) = integrator([](Eigen::VectorXd v){ return v; },
		                               [](Eigen::VectorXd x){ return Eigen::Vector2d(x[0] + 2 * x[0] * x[1], x[1] + x[0] * x[0] - x[1] * x[1]); },
		                               x0, v0,
		                               N, t0, T);

		x0 = x[N];
		v0 = v[N];

		if (block == 0)
		{
			sprintf(name, "trajectory-%.0f.dat", pE);
			auto filet = std::fopen(name, "w");
			for (size_t i = 0; i <= trajectory_N; i++)
			{
				std::fprintf(filet, "%.20f %.20f %.20f %.20f %.20f\n", t[i], x[i][0], x[i][1], v[i][0], v[i][1]);
			}
			std::fclose(filet);
		}

		for (size_t i = 1; i <= N; i++)
		{
			if (v[i][0] > 0 && x[i][0] * x[i - 1][0] < 0)
			{
				double t1 = t[i - 1];
				double t2 = t[i];
				double x1 = x[i - 1][0];
				double x2 = x[i][0];
				double y1 = x[i - 1][1];
				double y2 = x[i][1];

				double v_x = (x2 - x1) / (t2 - t1);
				double t_x = -x1 / v_x;

				double y_x = y1 + (y2 - y1) / (t2 - t1) * t_x;
				double vy_x = v[i - 1][1] + (v[i][1] - v[i - 1][1]) / (t2 - t1) * t_x;

				std::fprintf(file, "%.20f %.20f %.20f\n", t_x, y_x, vy_x);
			}
		}
	}
	std::fclose(file);
}

int main()
{
	std::vector<double> pE{12, 9, 8, 6};
	#pragma omp parallel for
	for (size_t i = 0; i < pE.size(); i++)
	{
		henonHeiles(symplectic4, pE[i]);
	}
	return 0;
}
