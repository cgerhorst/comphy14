CC = g++
CXX = g++
CXXFLAGS = -Wall -Wextra -Wpedantic -Wno-unused-but-set-variable -Wno-unused-parameter -Wno-sign-compare -g -std=c++11 -fopenmp -O2 $(shell pkg-config --cflags eigen3 | sed "s/-I/-isystem/g")
LDLIBS = -lstdc++ -lm -lgomp

HARMONIC = forwardEuler rungeKutta4 symplecticEuler verlet symplectic4
HARMONIC_DAT = $(addprefix harmonic-, $(addsuffix .dat, $(HARMONIC)))

HENON_HEILES = 12 9 8 6
HENON_HEILES_DAT = $(addsuffix .dat, $(addprefix poincare-, $(HENON_HEILES)) $(addprefix trajectory-, $(HENON_HEILES)))
HENON_HEILES_PDF = $(addsuffix .pdf, $(addprefix poincare-, $(HENON_HEILES)) $(addprefix trajectory-, $(HENON_HEILES)))

all: harmonic.pdf $(HENON_HEILES_PDF)

rungeKutta.o: rungeKutta.h

symplectic.o: symplectic.h

harmonic.o: rungeKutta.h symplectic.h

harmonic: rungeKutta.o symplectic.o

$(HARMONIC_DAT): dummy-harmonic
	@

dummy-harmonic: harmonic
	./harmonic
	touch dummy-harmonic

harmonic.pdf: harmonic.py matplotlibrc $(HARMONIC_DAT)
	python3 $< $(HARMONIC)

henonHeiles.o: rungeKutta.h symplectic.h

henonHeiles: rungeKutta.o symplectic.o

$(HENON_HEILES_DAT): dummy-henonHeiles
	@

dummy-henonHeiles: henonHeiles
	./henonHeiles
	touch dummy-henonHeiles

poincare-%.pdf: poincare.py matplotlibrc poincare-%.dat
	python3 $< $*

trajectory-%.pdf: trajectory.py matplotlibrc trajectory-%.dat
	python3 $< $*

clean:
	rm -f harmonic henonHeiles dummy-* *.o *.dat *.pdf

.PHONY: clean
